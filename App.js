import * as React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';  
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Account from './content/account';
import Chat from './content/chat';
import Play from './content/play';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
    <StatusBar hidden={true}/>
      <Tab.Navigator screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'Home') {
          if(focused)
          {
            return <MaterialCommunityIcons name='home' size={size} color={color}/>
          }
          else
          {
            return <MaterialCommunityIcons name='home-outline'  size={size} color={color}/>
          }
        } else if (route.name === 'Compte') {
          if(focused)
          {
            return <MaterialCommunityIcons name='account' size={size} color={color}/>
          }
          else
          {
            return <MaterialCommunityIcons name='account-outline' size={size} color={color}/>
          }
          iconName = focused ? 'account-box' : 'account';
        }
        else if (route.name ==='Chat'){
          if(focused)
          {
            return <MaterialCommunityIcons name='message' size={size} color={color}/>
          }
          else
          {
            return <MaterialCommunityIcons name='message-outline' size={size} color={color}/>
          }
        }
      },
    })}
    tabBarOptions={{
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    }}>
        <Tab.Screen name="Home" component={Play}/>
        <Tab.Screen name="Chat" component={Chat}/>
        <Tab.Screen name ="Compte" component={Account}/>
      </Tab.Navigator>
    </NavigationContainer>
  );
}
